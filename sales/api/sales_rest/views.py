from django.shortcuts import render
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
from .encoders import SalespersonEncoder, SaleEncoder, CustomerEncoder


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_salesperson(request, pk):
    try:
        if request.method == "DELETE":
            salesperson = Salesperson.objects.get(employee_id=pk)
            salesperson.delete()
            return JsonResponse({"message": "salesperson deleted"})
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"message": "Salesperson does not exist"},
            status=404,
        )


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_customer(request, pk):
    try:
        if request.method == "DELETE":
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse({"message": "customer deleted"})
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Customer does not exist"},
            status=404,
        )


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            setattr(automobile, "sold", True)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create the sale, invalid automobile"},
                status=400,
            )
        try:
            salesperson = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=salesperson)
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create the sale, invalid salesperson"},
                status=400,
            )
        try:
            customer = content["customer"]
            customer = Customer.objects.get(id=customer)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create the sale, invalid customer"},
                status=400,
            )

        price = content["price"]

        sale = Sale.objects.create(
            price=price,
            automobile=automobile,
            salesperson=salesperson,
            customer=customer,
        )

        automobile.sold = True
        automobile.save()

        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_sale(request, pk):
    try:
        if request.method == "DELETE":
            sale = Sale.objects.get(id=pk)
            sale.automobile.sold = False
            sale.delete()
            return JsonResponse({"message": "sale deleted"})
    except Sale.DoesNotExist:
        return JsonResponse(
            {"message": "Sale does not exist"},
            status=404,
        )
