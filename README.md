# Auto Center

## Overview
This project is a Full Stack web application designed to manage all inventory, sales, and service for an auto dealership. It provides a comprehensive solution for tracking inventory, managing sales, and assigning work orders to technicians. The application is built using Django, Python, JavaScript, React, PostgreSQL, HTML5, and CSS.

## Features
- Inventory Management: Track all inventory items, including vehicles, parts, and accessories.

- Sales Management: Manage sales transactions, including vehicle sales, financing, and customer information.

- Service Management: Assign work orders to technicians, track service requests, and manage service appointments.

- User Interface: An interactive user interface enables users to perform various tasks efficiently.

## Project Details
- **Tech Stack**: Django, Python, JavaScript, React, PostgreSQL, HTML5, CSS
- **Repository**: [Gitlab Repo](https://gitlab.com/Efren.Acosta/auto-center)

## Key Contributions
- Inherited a single-page application (SPA) architecture and seamlessly implemented pagination by routing all components using React Router.

- Developed an interactive user interface using React, enabling users to assign work orders to technicians and efficiently manage inventory.

## Installation and Usage
To run the application locally, follow these steps:
1. Clone the repository from GitLab.
2. Install dependencies using `npm install` and `pip install`.
3. Set up the PostgreSQL database and configure the database settings in the Django settings file.
4. Run migrations using `python manage.py migrate`.
5. Start the Django server using `python manage.py runserver`.
6. Start the React development server using `npm start`.
7. Access the application in your web browser at `http://localhost:3000`.

## Screenshots
[![Service Appointments](image.png)]
![Add A Customer](image-2.png)
![Add An Automobile](image-3.png)
![Models List](image-4.png)




## Contributors
- [Efren Acosta](https://www.linkedin.com/in/efren-acosta/) - Role (Full Stack Engineer/ Service Side)
- [Sydney Landers](https://www.linkedin.com/in/sydneymlanders/) - Role (Full Stack Engineer/ Sales Side)

