import React, { useEffect, useState } from "react";


export default function AppointmentForm() {
  const [appointments, setAppointments] = useState([]);
  const [technicians, setTechnicians] = useState([]);

  const [formData, setFormData] = useState({
    vin: "",
    customer: "",
    date_time: "",
    technician: "",
    reason: "",
  });

  const fetchAppointmentData = async () => {
    const appointment_url = "http://localhost:8080/api/appointments/";
    const response = await fetch(appointment_url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  const fetchTechnicianData = async () => {
    const technician_url = "http://localhost:8080/api/technicians/";
    const response = await fetch(technician_url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    fetchAppointmentData();
    fetchTechnicianData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8080/api/appointments/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        vin: "",
        customer: "",
        date_time: formData.date_time,
        technician: "",
        reason: "",
      });
    }
  };

  const handleFormChange = (e) => {
    const value =
      e.target.type === "datetime-local"
        ? e.target.valueAsNumber
        : e.target.value;
    const inputName = e.target.name;

    if (inputName === "date_time") {
      const date = new Date(value);
      const formattedDate = date.toISOString().slice(0, 16);
      setFormData({
        ...formData,
        [inputName]: formattedDate,
      });
    } else {
      setFormData({
        ...formData,
        [inputName]: value,
      });
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a service appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Vin"
                value={formData.vin}
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">Automobile VIN</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="customer"
                value={formData.customer}
                required
                type="text"
                name="customer"
                id="customer"
                className="form-control"
              />
              <label htmlFor="customer">Customer</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.date_time}
                required
                type="datetime-local"
                name="date_time"
                id="date_time"
                className="form-control"
              />
              <label htmlFor="date_time">Date/Time</label>
            </div>

            <div className="mb-3">
              <select
                onChange={handleFormChange}
                required
                name="technician"
                id="technician"
                value={formData.technician}
                className="form-select"
              >
                <option value="">Choose a technician</option>
                {technicians.map((technician) => {
                  return (
                    <option key={technician.employee_id} value={technician.id}>
                      {technician.employee_id}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="reason"
                value={formData.reason}
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="reason">Reason</label>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
