import { useEffect, useState } from "react";


export default function ModelsList() {
  const [models, setModels] = useState([]);
  const fetchData = async () => {
    const model_url = "http://localhost:8100/api/models/";
    const response = await fetch(model_url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (model) => {
    const url = `http://localhost:8100/api/models/${model.id}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      fetchData();
    }
  };

  return (
    <>
      <h1>Models</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img
                    src={model.picture_url}
                    alt={model.name + "picture"}
                    width={100}
                    height={100}
                  />
                </td>
                <td>
                <button onClick={() => handleDelete(model)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
