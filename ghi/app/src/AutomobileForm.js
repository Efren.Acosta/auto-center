import React, { useEffect, useState } from "react";


export default function AutomobileForm() {
  const [automobiles, setAutomobiles] = useState([]);
  const [models, setModels] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);

  const [formData, setFormData] = useState({
    color: "",
    year: "",
    vin: "",
    model: "",
  });

  const fetchAutomobileData = async () => {
    const automobile_url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(automobile_url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.automobiles);
    }
  };

  const fetchModelData = async () => {
    const model_url = "http://localhost:8100/api/models/";
    const response = await fetch(model_url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  const fetchManufacturerData = async () => {
    const manufacturer_url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(manufacturer_url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchAutomobileData();
    fetchModelData();
    fetchManufacturerData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8100/api/automobiles/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        color: "",
        year: "",
        vin: "",
        model: "",
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an automobile to inventory</h1>
          <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Color"
                value={formData.color}
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="year"
                value={formData.year}
                required
                type="text"
                name="year"
                id="year"
                className="form-control"
              />
              <label htmlFor="year">Year</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.vin}
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">VIN</label>
            </div>

            <div className="mb-3">
              <select
                onChange={handleFormChange}
                required
                name="model_id"
                id="model_id"
                value={formData.model_id}
                className="form-select"
              >
                <option value="">Choose a model</option>
                {models.map((model) => {
                  return (
                    <option key={model.id} value={model.id}>
                      {model.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
