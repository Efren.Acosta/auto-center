import React, { useState, useEffect } from "react";


export default function SalesHistory() {
  const [salespeople, setSalespeople] = useState([]);
  const [person, setPerson] = useState("");
  const [sales, setSales] = useState([]);

  const handleSalesperson = (event) => {
    const value = event.target.value;
    setPerson(value);
  };

  const fetchSalespeople = async () => {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  };

  const fetchSales = async () => {
    const url = "http://localhost:8090/api/sales/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  };

  useEffect(() => {
    fetchSalespeople();
    fetchSales();
  }, [person]);

  const filterSales = () => {
    const filtered = sales.filter(
      (sale) => sale.salesperson.employee_id === parseInt(person)
    );
    return filtered;
  };
  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Sales History</h1>

            <div className="form-floating mb-3">
              <select
                onChange={handleSalesperson}
                placeholder="Salesperson"
                value={person}
                required
                id="text"
                name="salesperson"
                className="form-select"
              >
                <option value="">Choose a salesperson</option>
                {salespeople.map((salesperson) => {
                  return (
                    <option
                      key={salesperson.employee_id}
                      value={salesperson.employee_id}
                    >
                      {salesperson.first_name} {salesperson.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>Car vin</th>
            <th>Sale Price</th>
          </tr>
        </thead>
        <tbody>
          {filterSales().map((sale) => {
            return (
              <tr key={sale.id}>
                <td>
                  {sale.salesperson.first_name}
                  <span> </span>
                  {sale.salesperson.last_name}
                </td>
                <td>
                  {sale.customer.first_name}
                  <span> </span>
                  {sale.customer.last_name}
                </td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
