import React, { useState, useEffect } from "react";


export default function ServiceHistory() {
  const [autos, setAutos] = useState([]);
  const [auto, setAuto] = useState([]);
  const [appointments, setAppointments] = useState([]);

  const handleAuto = (event) => {
    const value = event.target.value;
    setAuto(value);
  };

  const fetchAppointments = async () => {
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  const fetchAutos = async () => {
    try {
      const url = "http://localhost:8100/api/automobiles/";
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setAutos(data.autos);
      } else {
        console.error("Failed to fetch automobiles data.");
      }
    } catch (error) {
      console.error("An error occurred during the fetch call.");
    }
  };

  useEffect(() => {
    fetchAutos();
    fetchAppointments();
  }, [auto]);

  const filterAppointments = () => {
    const filtered = appointments.filter(
      (appointment) => appointment.auto.vin === parseInt(auto)
    );
    return filtered;
  };

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Service History</h1>

            <div className="form-floating mb-3">
              <select
                onChange={handleAuto}
                value={auto}
                required
                id="text"
                name="auto"
                className="form-select"
              >
                <option value="">Choose an automobile</option>
                {autos.map((auto) => (
                  <option key={auto.vin} value={auto.vin}>
                    {auto.manufacturer} {auto.model}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date/Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filterAppointments().map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.automobile.vin}</td>
                <td>{appointment.isVIP ? "Yes" : "No"}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date_time}</td>
                <td>
                  {appointment.technician.first_name}{" "}
                  {appointment.technician.last_name}
                </td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
