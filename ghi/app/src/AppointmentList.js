import { useEffect, useState } from "react";

export default function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  const fetchData = async () => {
    const appointment_url = "http://localhost:8080/api/appointments/";
    const response = await fetch(appointment_url);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const formatDate = (dateTimeString) => {
    const date = new Date(dateTimeString);
    const fields = {
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
      hour12: true,
    };
    const formattedDate = date.toLocaleString(undefined, fields);
    return formattedDate;
  };

  const cancelAppointment = async (appointment) => {
    const url = `http://localhost:8080/api/appointments/${appointment.id}/cancel/`;
    const body = { status: "Cancelled" };
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(body),
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      fetchData();
    }
  };

  const finishAppointment = async (appointment) => {
    const url = `http://localhost:8080/api/appointments/${appointment.id}/finish/`;
    const body = { status: "Finished" };
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(body),
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      fetchData();
    }
  };

  const handleDelete = async (appointment) => {
    const url = `http://localhost:8080/api/appointments/${appointment.id}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      fetchData();
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Customer Name</th>
          <th>Date/Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment) => {
          return (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer}</td>
              <td>{formatDate(appointment.date_time)}</td>
              <td>
                {appointment.technician.first_name}{" "}
                {appointment.technician.last_name}
              </td>

              <td>{appointment.reason}</td>
              <td>
                <button onClick={() => cancelAppointment(appointment)}>
                  Cancel
                </button>
              </td>
              <td>
                <button onClick={() => finishAppointment(appointment)}>
                  Finish
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
