import { useEffect, useState } from "react";


export default function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([]);
  const fetchData = async () => {
    const manufacturer_url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(manufacturer_url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (manufacturer) => {
    const url = `http://localhost:8100/api/manufacturers/${manufacturer.id}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      fetchData();
    }
  };



  return (
    <>
      <h1>Manufacturers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manufacturer) => {
            return (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
                <td><button onClick={() => handleDelete(manufacturer)}>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
