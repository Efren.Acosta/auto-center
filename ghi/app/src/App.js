import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SalespersonList from "./ListSalespeople";
import SalespersonForm from "./SalespersonForm";
import SaleList from "./ListSales";
import CustomerForm from "./CustomerForm";
import TechnicianForm from "./TechnicianForm";
import TechnicianList from "./TechnicianList";
import AppointmentForm from "./AppointmentForm";
import AppointmentList from "./AppointmentList";
import ModelForm from "./ModelForm";
import CustomerList from "./ListCustomers";
import ManufacturersList from "./ListManufacturers";
import ModelsList from "./ListModels";
import ManufacturerForm from "./ManufacturerForm";
import AutomobileList from "./ListAutomobiles";
import SalesHistory from "./SalespersonHistory";
import NewSaleForm from "./NewSaleForm";
import AutomobileForm from "./AutomobileForm";
import ServiceHistory from "./ServiceHistory";


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/models" element={<ModelsList />} />
          <Route path="/salespeople" element={<SalespersonList />} />
          <Route path="/salespeople/new" element={<SalespersonForm />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/sales" element={<SaleList />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/new" element={<AppointmentForm />} />
          <Route path="/models/create" element={<ModelForm />} />
          <Route path="/sales/new" element={<NewSaleForm />} />
          <Route path="/sales/history" element={<SalesHistory />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
          <Route path="/automobiles/history" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
