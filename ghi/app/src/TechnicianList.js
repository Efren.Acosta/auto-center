import { useEffect, useState } from "react";


export default function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);

  const fetchData = async () => {
    const hat_url = "http://localhost:8080/api/technicians/";
    const response = await fetch(hat_url);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleDelete = async (technician) => {
    const url = `http://localhost:8080/api/technicians/${technician.id}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      fetchData();
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map((technician) => {
          return (
            <tr key={technician.id}>
              <td>{technician.employee_id}</td>
              <td>{technician.first_name}</td>
              <td>{technician.last_name}</td>
              <td>
                <button onClick={() => handleDelete(technician)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
