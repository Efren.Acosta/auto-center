import React, { useEffect, useState } from "react";


export default function ModelForm() {
  const [models, setModels] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);

  const [formData, setFormData] = useState({
    name: "",
    picture_url: "",
    manufacturer_id: "",
  });

  const fetchModelData = async () => {
    const model_url = "http://localhost:8100/api/models/";
    const response = await fetch(model_url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  const fetchManufacturerData = async () => {
    const manufacturer_url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(manufacturer_url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchModelData();
    fetchManufacturerData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setFormData({
        name: "",
        picture_url: "",
        manufacturer_id: "",
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Vehicle Model</h1>
          <form onSubmit={handleSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="name"
                value={formData.name}
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Model name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Picture_url"
                value={formData.picture_url}
                required
                type="text"
                name="picture_url"
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleFormChange}
                required
                name="manufacturer_id"
                id="manufacturer_id"
                value={formData.manufacturer_id}
                className="form-select"
              >
                <option value="">Choose a manufacturer</option>
                {manufacturers.map((manufacturer) => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>
                      {manufacturer.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
